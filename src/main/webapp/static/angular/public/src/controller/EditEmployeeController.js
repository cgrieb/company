/**
 * Created by cgrieb on 3/15/16.
 */
angular.module('JpaApp').controller('EditEmployeeController',['$scope','$window','EmployeeService', function($scope,$window,EmployeeService){
    $scope.employee = {};

    initializeEditEdpController();

    function initializeEditEdpController() {
        $scope.employee = EmployeeService.getEmployee();
        if($scope.employee.length === 0) {
            $window.location = "/welcome";
        }

        $scope.cancelEmpEdit = function() {
            $window.location = "/employeeListing";
        };
    }

    $scope.submitEditedEmployee = function() {
        EmployeeService.employeeSave($scope.employee.id,$scope.employee.login,$scope.employee.name,
            $scope.employee.position,$scope.employee.salary).then(function(response){
            if(!response) {
                console.warn("** employee data does not match - possible hibernate issue **");
            }
            $window.location = "/employeeListing";
        });
    };

    $scope.setEnabledDisabled = function() {
        if($scope.admin.enabled === true) {
            return "true";
        } else {
            return "false";
        }
    };
}]);