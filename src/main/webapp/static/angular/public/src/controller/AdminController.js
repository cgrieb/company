/**
 * Created by cgrieb on 3/17/16.
 */
angular.module('JpaApp').controller('AdminController', ['$scope','$window','$location','AdminService', function($scope,$window,$location,AdminService){
    $scope.admins = {};
    $scope.roles = [];
    $scope.username = "";
    $scope.password = "";
    $scope.role = "";
    $scope.adminsLoaded = false;

    initializeAdminController();

    function initializeAdminController() {
        AdminService.getRoles().then(function(response){
            $scope.roles = response;
        });

        AdminService.getAllAdmins().then(function(response){
            $scope.admins = response;
            if($scope.admins.length > 0 ) {
                $scope.adminsLoaded = true;
            }
        });
    }

    $scope.getAllAdmins = function() {
        AdminService.getAllAdmins().then(function(response){
            $scope.admins = response;
        });
    };

    $scope.navigateToEdit = function(admin) {
        AdminService.setAdmin(admin);
        $location.path("/adminEdit");
    };

    $scope.navigateToAdmins = function() {
        $location.path("/adminListing");
    }
}]);