/**
 * Created by cgrieb on 4/6/16.
 */
angular.module('JpaApp').controller('LogoutController', ['AuthenticationService','$window',function(AuthenticationService,$window){
    $scope.executeLogout = function() {
        AuthenticationService.secureLogout().then(function(response){
           if(response === false) {
               console.log("** user logged out **");
           }
        });
        $window.location = "/";
    };
}]);