/**
 * Created by cgrieb on 5/13/16.
 */
angular.module('JpaApp').controller('EditAdminController',['$scope','$window','AdminService', function($scope,$window,AdminService){
    /**
     * Get the set admin object.
     */
    $scope.admin = AdminService.getAdmin();
    $scope.viewPassword = "123456";
    $scope.hehe = "1234567";

    /**
     * Return out if we haven't initialized anything yet.
     */
    if($scope.admin.length === 0) {
        $window.location = "/adminListing";
    }

    $scope.submitEditedAdmin = function() {
        if($scope.viewPassword !== "123456") {
            $scope.admin.password = $scope.viewPassword;
        }
        AdminService.saveUpdatedAdmin($scope.admin).then(function(response){
            console.log(response);
        });
        $window.location = "/adminListing";
    };

    $scope.setEnabledDisabled = function() {
        if($scope.admin.enabled === true) {
            return "true";
        } else {
            return "false";
        }
    };

    $scope.cancelEdit = function() {
        $window.location = "/adminListing";
    };
}]);