/**
 * Created by cgrieb on 3/4/16.
 */
angular.module('JpaApp').controller('WelcomeController', ['$scope','$uibModal','CmsService','AuthenticationService','PagingService', function($scope,$uibModal,CmsService,AuthenticationService,PagingService){
    $scope.types = [];
    $scope.items = [];

    $scope.filteredItems = [];

    $scope.item = {};

    $scope.isAdmin = false;

    /**
     * Paging variables are setup here.
     */
    $scope.itemsPerPage = 3;
    $scope.currentPage = 1;

    $scope.totalItems = 0;

    $scope.searchValue = "";

    initializeWelcomeCtrl();

    function initializeWelcomeCtrl() {
        CmsService.getCmsTypes().then(function(response){
            $scope.types = response;
        });

        CmsService.getAllCmsItems().then(function(response){
            $scope.items = response;
            $scope.totalItems = $scope.items.length;
            PagingService.setPageItems(3,1,$scope.items);
            $scope.filteredItems = PagingService.getFilteredItems(0,3);
        });

        AuthenticationService.getAdminStatus().then(function(response){
            $scope.isAdmin = response;
        });
    }

    $scope.openAddContent = function (item) {
        $scope.item = item;
        var modalInstance = $uibModal.open({
            templateUrl: 'add-cms-content.html',
            scope: $scope,
            controller: 'ModalController'
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            console.info("** no cms content added/saved **");
        });
    };

    $scope.getDateTime = function(time) {
        var date = new Date(time);
        return date.toString();
    };

    $scope.pageChanged = function() {
        $scope.filteredItems = PagingService.updatePage($scope.currentPage);
    };

    $scope.executeSearch = function() {
        if($scope.searchValue.length > 1) {
            CmsService.itemSearch($scope.searchValue).then(function(response){
                $scope.totalItems = response.length;
                PagingService.setPageItems(3,1,response);
                $scope.filteredItems = PagingService.getFilteredItems(0,3);
                $scope.searchValue = "";
            });
        }
    };
}]);