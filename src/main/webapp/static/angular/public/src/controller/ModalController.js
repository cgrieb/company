/**
 * Created by cgrieb on 4/17/16.
 */
angular.module('JpaApp').controller('ModalController', ['$scope','$uibModalInstance','$window','CmsService',function($scope,$uibModalInstance,$window,CmsService){
    $scope.theseTypes = $scope.types;
    $scope.type = "welcome-story";

    /**
     * Note: we'll need different variables for different types of
     * CMS items - for now, we just have the one. Move this into
     * a CmsItemService in the future.
     */
    $scope.live = true;
    $scope.published = true;
    $scope.data = "";
    $scope.title = "";
    $scope.id = 0;

    $scope.itemExists = false;

    $scope.maxTitle = 200;
    $scope.maxData = 4000;

    if($scope.item != null) {
       setItem();
    }

    var previousTitleLength = $scope.title.length;
    var previousDataLength = $scope.data.length;

    $scope.save = function() {
        if($scope.itemExists) {
            CmsService.saveExistingItem($scope.data,$scope.id,$scope.live,
                $scope.published,$scope.title).then(function(response){
                    if($scope.id == response) {
                        console.log("** item #" + $scope.id + " has been updated **");
                    } else {
                        console.warn("** possible hibernate transaction failure **");
                    }
                    $window.location = "/welcome";
                });
        } else {
            CmsService.saveCmsItem($scope.data,$scope.live,
                $scope.published,$scope.title,$scope.type).then(function(response){
                    if($scope.id == response) {
                        console.log("** item #" + $scope.id + " has been added **");
                    } else {
                        console.warn("** possible hibernate transaction failure **");
                    }
                    $window.location = "/welcome";
                });
        }
        $uibModalInstance.close();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.updateMaxTitle = function() {
        $scope.maxTitle = calculateRemaining($scope.maxTitle,previousTitleLength,$scope.title.length);
        previousTitleLength = $scope.title.length;
    };

    $scope.updateMaxContent = function() {
        $scope.maxData = calculateRemaining($scope.maxData,previousDataLength,$scope.data.length);
        previousDataLength = $scope.data.length;
    };

    $scope.validateNewContent = function() {
        return($scope.title.length > 2 && $scope.data.length > 2)
    };

    function calculateRemaining(length, previous,current) {
        if (length > previous && previous == current) {
            length -= current;
            return length;
        }

        if(previous > current) {
            length += (previous - current);
            return length;
        }

        if (length >= 0) {
            length += (previous - current);
            return length;
        }

        return 0;
    }

    function setItem() {
        $scope.itemExists = true;

        $scope.data = $scope.item.data;
        $scope.title = $scope.item.title;
        $scope.live = $scope.item.live;
        $scope.published = $scope.item.published;
        $scope.type = $scope.item.type;
        $scope.id = $scope.item.id;

        previousTitleLength = $scope.title.length;
        previousDataLength = $scope.data.length;

        $scope.maxTitle = calculateRemaining($scope.maxTitle,previousTitleLength,$scope.title.length);
        $scope.maxData = calculateRemaining($scope.maxData,previousDataLength,$scope.data.length);
    }
}]);