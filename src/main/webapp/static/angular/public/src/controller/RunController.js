/**
 * Created by cgrieb on 3/3/16.
 */
angular.module('JpaApp').controller('RunController',['AuthenticationService','EmployeeService','$location','$window','$scope','$route','$interval','$document', function(AuthenticationService,EmployeeService,$location,$window,$scope,$route,$interval,$document){
    $scope.authenticated = false;
    $scope.isAdmin = false;
    $scope.searchValue = "";

    var interval = 10;
    var maxInterval = 180;

    $interval(callAtInterval, 5000);

    executeRunController();

    function executeRunController() {
        var url = $location.url();

        AuthenticationService.isAuthenticated().then(function(response){
            if(response === true) {
                $scope.authenticated = true;
                AuthenticationService.getAuthorities().then(function(authorities){
                    for (var i = 0; i < authorities.length; i++) {
                        var authority = authorities[i];
                        if(authority.authority === "admin") {
                            $scope.isAdmin = true;
                        }
                    }
                    $location.path(url);
                });
            } else {
                $location.path("/login");
            }
        });
    }

    $scope.executeLookup = function(searchValue) {
        EmployeeService.employeeLookup(searchValue).then(function(response){
            EmployeeService.setEmployees(response);
            if($location.path() === "/employeeLookup") {
                $route.reload();
            } else {
                $location.path("/employeeLookup");
            }
        });
    };

    $scope.executeLogout = function() {
        AuthenticationService.secureLogout().then(function(response){
            if(response === false) {
                console.log("** user logged out **");
                $window.location = "/login";
            } else {
                console.log("** there was an error logging out **");
            }
        });
    };

    function callAtInterval() {
       if($scope.authenticated) {
           if(interval>maxInterval) {
               $scope.executeLogout();
               interval = 0;
           } else {
               interval++;
           }
       }
    }

    /**
     * For now, we're just responding to mouse movements.
     */
    $document.find('body').on("mousemove", function(){
        interval = 0;
    });
}]);