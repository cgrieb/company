/**
 * Created by cgrieb on 4/4/16.
 */
angular.module('JpaApp').controller('CacheController', ['CacheService', function(CacheService){
    CacheService.clearCache();
}]);