/**
 * Created by cgrieb on 4/4/16.
 */
angular.module('JpaApp').controller('HomeController', ['$scope','AuthenticationService', function($scope,AuthenticationService){
    $scope.userDetails = {};
    AuthenticationService.getUserDetails().then(function(response){
        $scope.userDetails = response;
        var authorities = response.authorities;
        $scope.userDetails.authority = authorities[0].authority;
    });
}]);