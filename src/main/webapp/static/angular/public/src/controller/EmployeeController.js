/**
 * Created by cgrieb on 3/9/16.
 */
angular.module('JpaApp').controller('EmployeeController', ['$scope','$window','$location','EmployeeService','PagingService', function($scope,$window,$location,EmployeeService,PagingService){
    $scope.employees = [];
    $scope.filteredEmployees = [];
    $scope.filter = "";

    $scope.fistImgEnable = false;
    $scope.secondImgEnable = false;

    $scope.firstAvatar = "/static/angular/public/img/user/male_user.png";
    $scope.secondAvatar = "/static/angular/public/img/user/female_user.png";

    $scope.avatar = "";

    $scope.employeeDataLoaded = false;

    /**
     * Paging variables are setup here.
     */
    $scope.itemsPerPage = 5;
    $scope.currentPage = 1;

    $scope.totalItems = 0;

    initEmployeeController();

    function initEmployeeController() {
        var path = $location.url();
        if(path==="/employeeLookup") {
            $scope.employees = EmployeeService.getEmployees();
            setFilteredAndPagedItems();
        }

        EmployeeService.getAllEmployees().then(function(response){
            $scope.employees = response;
            setFilteredAndPagedItems();
        });
    }

    $scope.getSearchResults = function() {
        $scope.employees = EmployeeService.getEmployees();
    };
    
    $scope.executeLookup = function(value) {
        if($scope.isEmployeesEmpty() > 0) {
            $scope.employees = {};
        }
        EmployeeService.employeeLookup(value).then(function(response){
            $scope.employees = response;
        });
        $scope.value = null;
    };

    $scope.isEmployeesEmpty = function() {
        return Object.keys($scope.employees).length;
    };

    $scope.submitNewEmployee = function() {
        EmployeeService.employeeAdd($scope.avatar, $scope.name,$scope.salary, $scope.position,$scope.login).then(function(response){
            if(!response) {
                console.info("** possible hibernate related issue with adding employee " + $scope.login + " **");
            }
            $window.location = "/employeeListing";
        });
    };

    $scope.removeEmployee = function(id) {
       EmployeeService.employeeRemove(id).then(function(response){
           if(!response) {
               console.log("** warning: employee with the ID of " + id + " possible failure to remove");
           } else {
               $scope.$apply();
           }
       });
    };

    $scope.navigateToEdit = function(employee) {
        EmployeeService.setEmployee(employee);
        $location.path("/employeeEdit");
    };

    $scope.resetFields = function() {
        $scope.avatar = "";
        $scope.name = "";
        $scope.salary = "";
        $scope.login = "";
        $scope.position = "";
        $scope.fistImgEnable = false;
        $scope.secondImageEnabled = false;
    };

    $scope.toggle = function(image) {
        $scope.avatar = image;
        if(image === $scope.firstAvatar) {
            $scope.fistImgEnable = true;
            $scope.secondImgEnable = false;
        } else {
            $scope.secondImgEnable = true;
            $scope.fistImgEnable = false;
        }
    };

    $scope.validateForm = function() {
        return ($scope.avatar.length !== 0);
    };

    $scope.pageChanged = function() {
        $scope.filteredEmployees = PagingService.updatePage($scope.currentPage);
    };

    function setFilteredAndPagedItems() {
        $scope.totalItems = $scope.employees.length;
        PagingService.setPageItems(5,1,$scope.employees);
        $scope.filteredEmployees = PagingService.getFilteredItems(0,5);
        $scope.employeeDataLoaded = true;
    }
}]);