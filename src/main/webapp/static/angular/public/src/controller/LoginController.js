/**
 * Created by cgrieb on 3/3/16.
 */
angular.module('JpaApp').controller('LoginController',['$scope','$window','AuthenticationService',function($scope,$window,AuthenticationService){
    $scope.loginSuccessful = true;

    $scope.login = function() {
        if(!$scope.loginSuccessful) {
            $scope.loginSuccessful = true;
        }
        AuthenticationService.secureLogin($scope.username,$scope.password).then(function(response){
            if(response === false) {
                $scope.loginSuccessful = false;
                resetUserPassField();
            } else {
                $window.location = "/welcome";
            }
        });
    };

    function resetUserPassField() {
        $scope.username = "";
        $scope.password = "";
    }
}]);