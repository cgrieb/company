/**
 * Created by cgrieb on 10/26/16.
 */
angular.module('JpaApp').controller('AddAdminController', ['AdminService','$scope','$window', function(AdminService,$scope,$window){
    $scope.username = "";
    $scope.password = "";
    $scope.enabled = true;
    $scope.role = "user";

    $scope.submitNewAdmin = function() {
        AdminService.addAdmin($scope.password,$scope.role,$scope.username).then(function(response){
            console.log("** admin " + $scope.username + " added to the admin group: " + response);
        });
        $window.location = "/adminListing";
    };
}]);