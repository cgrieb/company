/**
 * Created by cgrieb on 3/17/16.
 */
angular.module('JpaApp').service('AdminService',['$q','$http', function($q,$http){
    this.admin = [];
    var TIMEOUT = 2000;

    this.getAllAdmins = function() {
        var deferred = $q.defer();
        $http({
            url: '/admin/getAllAdmins',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
           deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.addAdmin = function(password,role,username) {
        var deferred = $q.defer();
        $http({
            url:'/admin/adminAdd',
            method: 'POST',
            params:{password:password,role:role,username:username},
            timeout:TIMEOUT
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.saveUpdatedAdmin = function(admin) {
        var deferred = $q.defer();
        $http({
            url: '/admin/adminSave',
            method:'POST',
            timeout:TIMEOUT,
            params:{enabled:admin.enabled,password:admin.password,role:admin.role,username:admin.username}
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.getRoles = function() {
        var deferred = $q.defer();
        $http({
            url:'/admin/getAllRoles',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.setAdmin = function(admin) {
        this.admin = admin;
    };

    this.getAdmin = function() {
        return this.admin;
    };
}]);