/**
 * Created by cgrieb on 4/19/16.
 */
angular.module('JpaApp').service('CmsService', ['$http','$q',function($http,$q){
    var TIMEOUT = 2000;

    this.getCmsTypes = function() {
        var deferred = $q.defer();
        $http({
            url:'/cms/getCmsTypes',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.saveCmsItem = function(content,itemIsLive,publishItem,title,type) {
        var deferred = $q.defer();
        $http({
            url:'/cms/saveCmsItems',
            method:'POST',
            params:{content:content,itemIsLive:itemIsLive,publishItem:publishItem,title:title,type:type},
            timeout:TIMEOUT
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.saveExistingItem = function(data,id,live,published,title) {
        var deferred = $q.defer();
        $http({
            url: '/cms/saveExistingCmsItem',
            method:'POST',
            params:{data:data,id:id,live:live,published:published,title:title},
            timeout:TIMEOUT
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.getAllCmsItems = function() {
        var deferred = $q.defer();
        $http({
            url:'/cms/getCmsItems',
            method: 'GET',
            timeout:TIMEOUT
        }).success(function(response){
           deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.itemSearch = function(searchValue) {
        var deferred = $q.defer();
        $http({
            url:'/cms/cmsSearch',
            params:{searchValue:searchValue},
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
           deferred.resolve(response);
        });
        return deferred.promise;
    };
}]);