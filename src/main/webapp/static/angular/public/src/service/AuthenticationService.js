/**
 * Created by cgrieb on 3/4/16.
 */
angular.module('JpaApp').service('AuthenticationService', ['$http','$q', function($http, $q){
    var TIMEOUT = 2000;

    this.isAuthenticated = function() {
        var deferred = $q.defer();
        $http({
            method:'GET',
            url:'/auth/getAuthStatus',
            timeout: TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.secureLogin = function(user,pass) {
        var deferred = $q.defer();
        $http({
            method:'GET',
            url:'/auth/login',
            timeout: TIMEOUT,
            params:{j_username:user,j_password:pass}
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.secureLogout = function() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url:"/auth/logout",
            timeout:TIMEOUT
        }).success(function(response){
           deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getAuthorities = function() {
        var deferred = $q.defer();
        $http({
            method:'GET',
            url:'/auth/getUserDetails',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response.authorities);
        });
        return deferred.promise;
    };

    this.getAdminStatus = function() {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: '/auth/isAdmin',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getUserDetails = function() {
        var deferred = $q.defer();
        $http({
            url: '/auth/getUserDetails',
            timeout: TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    };
}]);