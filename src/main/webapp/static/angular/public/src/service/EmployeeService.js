/**
 * Created by cgrieb on 3/9/16.
 */
angular.module('JpaApp').service('EmployeeService', ['$q','$http', function($q,$http){
    this.employee = [];
    this.employees = [];

    var TIMEOUT = 2000;

    this.employeeLookup = function(value) {
        var deferred = $q.defer();
        $http({
            url: '/employee/employeeSearch',
            params:{value:value},
            timeout:TIMEOUT
        }).success(function(response){
           deferred.resolve(response);
        });
        return deferred.promise;
    };

    this.getAllEmployees = function() {
        var deferred = $q.defer();
        $http({
            url: '/employee/getAllEmployees',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response)
        });
        return deferred.promise;
    };

    this.employeeAdd = function(avatar,name,salary,position,login) {
        var params = {avatar:avatar,login:login,name:name,position:position,salary:salary};
        var deferred = $q.defer();
        $http({
            method:'POST',
            url: '/employee/employeeAdd',
            timeout:TIMEOUT,
            params:params
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.employeeRemove = function(id) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: '/employee/employeeRemove',
            params:{id:id},
            timeout:TIMEOUT
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.employeeSave = function(id,login,name,position,salary) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: '/employee/employeeSave',
            params:{id:id,login:login,name:name,position:position,salary:salary},
            timeout:TIMEOUT
        }).success(function(){
           deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false);
        });
        return deferred.promise;
    };

    this.setEmployee = function(employee) {
       this.employee = employee;
    };

    this.getEmployee = function() {
        return this.employee;
    };

    this.setEmployees = function(employees) {
        this.employees = employees;
    };

    this.getEmployees = function() {
        return this.employees;
    };
}]);