/**
 * Created by cgrieb on 5/8/16.
 */
angular.module('JpaApp').service('PagingService', [function(){
    this.itemsPerPage = 0;
    this.currentPage = 0;
    this.totalItems = 0;

    this.filteredItems = [];
    this.items = [];

    this.setPageItems = function(itemsPerPage,currentPage,items) {
        this.itemsPerPage = itemsPerPage;
        this.currentPage = currentPage;
        this.items = items;
    };

    this.getFilteredItems = function(start,end) {
        this.filteredItems = this.items.slice(start,end);
        return this.filteredItems;
    };

    this.updatePage = function(currentPage) {
        var begin = ((currentPage - 1) * this.itemsPerPage);
        var end = begin + this.itemsPerPage;
        return this.items.slice(begin,end);
    };
}]);