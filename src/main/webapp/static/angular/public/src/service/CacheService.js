/**
 * Created by cgrieb on 4/4/16.
 */
angular.module('JpaApp').service('CacheService', ['$http','$q', function($http,$q){
    var TIMEOUT = 2000;
    this.clearCache = function() {
        $http({
            url: '/admin/clearCache',
            timeout: TIMEOUT,
            method:'GET'
        }).success(function(){
            console.log("** System cache has been cleared **");
        }).error(function(){
            console.log("** There was an error clearing system cache **");
        });
    };
}]);