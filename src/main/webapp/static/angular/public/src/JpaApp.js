/**
 * Created by cgrieb on 3/3/16.
 */
angular.module('JpaApp', ['ngRoute','ngResource','ngMessages','angular-toArrayFilter','ui.bootstrap','ui.bootstrap.pagination'])
    .config(function($routeProvider,$locationProvider){

    /**
     * Setup our routes.
     */
    $routeProvider.when("/welcome",{
        controller:"WelcomeController",
        templateUrl:"/static/angular/public/view/welcome.html"
    }).when("/home", {
        controller:"HomeController",
        templateUrl: "/static/angular/public/view/home.html"
    }).when("/login", {
        controller:"LoginController",
        templateUrl:"/static/angular/public/view/login.html"
    }).when("/employeeLookup",{
        controller:"EmployeeController",
        templateUrl:"/static/angular/public/view/employee/search.html"
    }).when("/employeeListing", {
        controller:"EmployeeController",
        templateUrl:"/static/angular/public/view/employee/list.html"
    }).when("/employeeAddition", {
        controller:"EmployeeController",
        templateUrl: "/static/angular/public/view/employee/add.html"
    }).when("/employeeEdit", {
        controller:"EditEmployeeController",
        templateUrl:  "/static/angular/public/view/employee/edit.html"
    }).when("/adminListing", {
        controller:"AdminController",
        templateUrl: "/static/angular/public/view/admin/list.html"
    }).when("/adminEdit", {
        controller: "EditAdminController",
        templateUrl: "/static/angular/public/view/admin/edit.html"
    }).when("/adminAddition", {
       controller: "AdminController",
        templateUrl: "/static/angular/public/view/admin/add.html"
    }).when("/cacheClear", {
        controller: "CacheController",
        templateUrl: "/static/angular/public/view/cache.html"
    }).otherwise({
        redirectTo:"/"
    });

    /**
     * Get rid of hashtag links.
     */
    $locationProvider.html5Mode(true);
});