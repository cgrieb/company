package company.entity;

/**
 * Created by christopherg27 on 12/26/15.
 */
import javax.persistence.*;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SALARY")
    private double salary;
    @Column(name = "POSITION")
    private String position;
    @Column(name = "LOGIN")
    private String login;
    @Column(name = "AVATAR")
    private String avatar;

    public Employee(int id, String name, double salary, String position) {
        super( );
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.position = position;
    }

    public Employee( ) {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int eid) {
        this.id = eid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary( ) {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String deg) {
        this.position = deg;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
