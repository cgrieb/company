package company.entity;

import javax.persistence.*;

/**
 * Created by cgrieb on 1/6/16.
 */
@Entity
@Table(name = "role_listing")
public class Roles {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column(name = "role")
    private String role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Roles() {
        super();
    }
}
