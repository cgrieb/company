package company.entity;

import javax.persistence.*;

/**
 * Created by cgrieb on 4/25/16.
 */
@Entity
@Table(name = "cms_content_item")
public class CmsItem {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column(name = "creator")
    private String creator;

    @Column(name = "created")
    private long created;

    @Column(name = "type")
    private String type;

    @Column(name = "live")
    private boolean live;

    @Column(name = "published")
    private boolean published;

    @Column(name = "title")
    private String title;

    @Column(name = "data")
    private String data;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public boolean getPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
