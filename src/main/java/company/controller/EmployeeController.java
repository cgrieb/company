package company.controller;

import company.entity.Admin;
import company.entity.Employee;
import company.entity.Roles;
import company.service.AdminService;
import company.service.EmployeeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {
	@Resource
	EmployeeService employeeService;

    /**
     * Get all employees.
     * @return List of Employees.
     */
    @PreAuthorize("hasAnyRole('admin,user')")
    @RequestMapping(value = "/getAllEmployees", method = RequestMethod.GET)
    public @ResponseBody List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    /**
     * Employee Search results entry point.
     * @param value - Id of the user.
     * @return - List of Employees.
     */
    @PreAuthorize("hasAnyRole('admin,user')")
    @RequestMapping(value = "/employeeSearch", method = RequestMethod.GET)
    public @ResponseBody List<Employee> employeeSearch(@RequestParam(value = "value", required = true) String value) {
        return employeeService.findEmployee(value);
    }

    /**
     * Entry point for adding a new employee.
     * @param avatar - employee avatar.
     * @param name - employee name.
     * @param position - employee position.
     * @param salary - employee salary.
     */
    @PreAuthorize("hasAnyRole('admin,user')")
    @RequestMapping(value = "/employeeAdd", method = RequestMethod.POST)
    public void employeeAdd(@RequestParam(value = "avatar", required = true) String avatar, @RequestParam(value = "login", required = true) String login,
        @RequestParam(value = "name", required = true) String name, @RequestParam(value = "position", required = true) String position, @RequestParam(value = "salary", required = true) int salary) {
        employeeService.addEmployee(avatar,login,name,position,salary);
    }

    /**
     * Save an existing, edited employee.
     * @param id - ID of the employee (unique index).
     * @param login - login (user@email).
     * @param name - name "Bob Hunter"
     * @param position - position.
     * @param salary - salary.
     */
    @PreAuthorize("hasAnyRole('admin,user')")
    @RequestMapping(value = "/employeeSave", method = RequestMethod.POST)
    public void employeeSave(@RequestParam(value = "id", required = true) int id, @RequestParam(value = "login", required = true) String login,
        @RequestParam(value = "name", required = true) String name, @RequestParam(value = "position", required = true) String position, @RequestParam(value = "salary", required = true) int salary) {
        employeeService.saveEmployee(id, login, name, position, salary);
    }

    /**
     * Remove an employee entry point.
     * @param id - id of the user.
     */
    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/employeeRemove", method = RequestMethod.POST)
    public void removeEmployee(@RequestParam(value = "id", required = true) int id) {
        employeeService.removeEmployee(id);
    }
}