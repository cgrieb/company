package company.controller;

import company.entity.Admin;
import company.entity.Roles;
import company.service.AdminService;
import company.service.CacheService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by cgrieb on 10/22/16.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {
    @Resource
    private AdminService adminService;
    @Resource
    private CacheService cacheService;

    /**
     * Entry point for displaying all admins.
     * @return a listing of all admins.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/getAllAdmins", method = RequestMethod.GET)
    public @ResponseBody
    List<Admin> getAllAdmins() {
        return adminService.getAllAdmins();
    }

    /**
     * Get all roles.
     * @return A list of all roles.
     */
    @PreAuthorize("hasAnyRole('admin,user')")
    @RequestMapping(value = "/getAllRoles", method = RequestMethod.GET)
    public @ResponseBody List<Roles> getRoles() {
        return adminService.getRoleListing();
    }

    /**
     * Add a new admin account - entry point.
     * @param username admin username.
     * @param role admin role.
     * @param password admin password.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/adminAdd", method = RequestMethod.POST)
    public void adminAdd(@RequestParam(value = "password", required = true) String password, @RequestParam(value = "role", required = true) String role,
        @RequestParam(value = "username", required = true) String username) {
        adminService.adminAdd(password,role,username);
    }

    /**
     * Save an existing admin account entry point.
     * @param enabled - is the account enabled/disabled?
     * @param password - admin password.
     * @param role - user or an admin level account?
     * @param username - username of the admin.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/adminSave", method = RequestMethod.POST)
    public void adminSave(@RequestParam(value = "enabled", required = true) boolean enabled, @RequestParam(value = "password", required = true) String password,
        @RequestParam(value = "role", required = true) String role, @RequestParam(value = "username", required = true) String username) {
        adminService.saveAdmin(enabled,password,role,username);
    }

    /**
     * Entry point for clearing all system cache.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/clearCache", method = RequestMethod.GET)
    public void clearCache() {
        cacheService.clearCache();
    }
}
