package company.controller;

import company.service.AuthService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 10/22/16.
 */

@Controller
@RequestMapping(value = "/auth")
public class AuthController {
    @Resource
    private AuthService authService;


    /**
     * login - spring security redirected login page.
     * @param username - admin/user username.
     * @param password - admin/user password.
     * @return successful login, not successful login.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public @ResponseBody
    boolean login(@RequestParam(value = "j_username", required = true) String username, @RequestParam(value = "j_password", required = true) String password) {
        return authService.login(username,password);
    }

    /**
     * Get authenticated user status.
     * @return authenticated, not authenticated via boolean.
     */
    @RequestMapping(value = "/getAuthStatus", method = RequestMethod.GET)
    public @ResponseBody boolean getAuthStatus() {
        return authService.getAuthStatus();
    }

    /**
     * Logs out a user. Note: we can add this to the spring security page as a custom
     * logout url and remove this entry point.
     * @return redirect to /getAuthStatus - will return false.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String  logout() {
        authService.logout();
        return "redirect:/auth/getAuthStatus";
    }

    /**
     * Get logged in user userDetails.
     * @return UserDetails.
     */
    @PreAuthorize("hasAnyRole('admin,user')")
    @RequestMapping(value = "/getUserDetails", method = RequestMethod.GET)
    public @ResponseBody
    UserDetails getUserDetails() {
        return authService.getUserDetails();
    }

    /**
     * Entry point for determine if we're an admin.
     * @param request - HttpServletRequest.
     * @return true/false.
     */
    @PreAuthorize("hasAnyRole('admin,user')")
    @RequestMapping(value = "/isAdmin", method = RequestMethod.GET)
    public @ResponseBody boolean isAdmin(HttpServletRequest request) {
        return authService.isAdmin(request);
    }

}
