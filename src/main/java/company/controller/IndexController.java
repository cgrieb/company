package company.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by cgrieb on 10/22/16.
 */
@Controller
public class IndexController {

    /**
     * Primary index return entry mapping.
     * @return - index.html.
     */
    @RequestMapping(value = "/*", method = RequestMethod.GET)
    public String index() {
        return "index";
    }
}
