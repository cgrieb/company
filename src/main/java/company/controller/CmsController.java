package company.controller;

import company.entity.CmsItem;
import company.entity.CmsType;
import company.service.CmsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by cgrieb on 4/16/16.
 */
@Controller
@RequestMapping(value = "/cms")
public class CmsController {
    @Resource
    CmsService cmsService;

    /**
     * Get all CMS content types.cms_website
     * @return A listing of all CMS item types.
     */
    @PreAuthorize("hasAnyRole('admin','user')")
    @RequestMapping(value = "/getCmsTypes", method = RequestMethod.GET)
    public @ResponseBody List<CmsType> getCmsTypes() {
        return cmsService.getAllTypes();
    }

    /**
     * Save a new CMS item.
     * @param content - CM content.
     * @param itemIsLive - is this a live item?
     * @param publishItem - his is a published item?
     * @param title - title of the item we saving.
     * @param type - type of item.
     * @return value of the item we just saved.
     */
    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/saveCmsItems", method = RequestMethod.POST)
    public void saveCmsItems(@RequestParam(value = "content", required = true) String content, @RequestParam(value = "itemIsLive", required = true) boolean itemIsLive,
                        @RequestParam(value = "publishItem", required = true) boolean publishItem, @RequestParam(value = "title") String title,@RequestParam(value = "type") String type) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        cmsService.addContent(content,itemIsLive,publishItem,title,type,userDetails);
    }

    /**
     * Save an existing CMS item.
     * @param data - data.
     * @param id - item ID.
     * @param live - is this item live?
     * @param published - is this item published?
     * @param title - item title.
     * @return item id that we just saved.
     */
    @PreAuthorize("hasRole('admin')")
    @RequestMapping(value = "/saveExistingCmsItem", method = RequestMethod.POST)
    public void saveExistingItem(@RequestParam(value = "data") String data,@RequestParam(value = "id") int id,@RequestParam(value = "live") boolean live,
                                                 @RequestParam(value = "published") boolean published, @RequestParam(value = "title") String title) {
        cmsService.saveExistingItem(data,id,live,published,title);
    }

    /**
     * Perform a search on searchValue.
     * @param searchValue - value we're searching for.
     * @return A List of CmsItems.
     */
    @PreAuthorize(("hasAnyRole('admin','user')"))
    @RequestMapping(value = "/cmsSearch", method = RequestMethod.GET)
    public @ResponseBody List<CmsItem> getCmsItems(@RequestParam(value = "searchValue") String searchValue) {
        return cmsService.searchForItems(searchValue);
    }

    /**
     * Get all CM Items.
     * @return a listing of CmsItems.
     */
    @PreAuthorize(("hasAnyRole('admin','user')"))
    @RequestMapping(value = "/getCmsItems", method = RequestMethod.GET)
    public @ResponseBody List<CmsItem> getCmsItems() {
        return cmsService.getAllCmsItems();
    }
}