package company.encoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by cgrieb on 1/3/16.
 */
public class StringToSHA256Encoder {
    public String encode(String password) {
        StringBuffer hexBuffer = new StringBuffer();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(password.getBytes());
            byte bytes[] = digest.digest();

            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                buffer.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            hexBuffer = new StringBuffer();
            for (int i=0;i<bytes.length;i++) {
                String hex=Integer.toHexString(0xff & bytes[i]);
                if(hex.length()==1) hexBuffer.append('0');
                hexBuffer.append(hex);
            }
        } catch (NoSuchAlgorithmException e) {

        }
        return hexBuffer.toString();
    }
}
