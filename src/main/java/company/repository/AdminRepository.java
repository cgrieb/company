package company.repository;

import company.entity.Admin;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 1/13/16.
 */
@Repository
public interface AdminRepository extends JpaRepository<Admin,String> {
    /**
     * Determine if an admin already exists.
     * @param username admin username.
     * @return Admin object, or a null value if account is not found.
     */
    @Cacheable(value = "adminCache", key = "#username")
    @Query("select admin from Admin admin where admin.username =:username")
    Admin getAdminFromUsername(String username);

    /**
     * Obtain a listing of all admins.
     * @return List of admins.
     */
    @Cacheable(value = "adminCache", key = "#root.targetClass")
    @Query("select admin from Admin admin")
    List<Admin> getAllAdmins();
}
