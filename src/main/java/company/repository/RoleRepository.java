package company.repository;

import company.entity.Roles;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 4/13/16.
 */
@Repository
public interface RoleRepository extends JpaRepository<Roles,String> {
    /**
     * Get a listing of roles.
     * @return a listing List<> of all roles.
     */
    @Cacheable(value = "roleCache", key = "#root.targetClass")
    @Query(value = "select role from Roles")
    List<Roles> getRoles();
}
