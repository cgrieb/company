package company.repository;

import company.entity.CmsItem;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 4/28/16.
 */
@Repository
public interface CmsItemRepository extends JpaRepository<CmsItem,Integer> {
    /**
     * Search for various CMS items via title.
     * @param searchValue - value we're searching for.
     * @return a listing of located items.
     */
    @Query("select item from CmsItem item where item.title like:searchValue")
    List<CmsItem> searchForItems(@Param(value = "searchValue") String searchValue);

    /**
     * Get all CMS items.
     * @return A listing of all CMS items.
     */
    @Cacheable(value = "cmsCache", key = "#root.targetClass")
    @Query("select item from CmsItem item order by item.published desc")
    List<CmsItem> getAllItems();
}
