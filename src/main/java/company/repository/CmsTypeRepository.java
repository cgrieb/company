package company.repository;

import company.entity.CmsType;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 4/18/16.
 */
@Repository
public interface CmsTypeRepository extends JpaRepository<CmsType,String> {
    /**
     * Get all CMS data types.
     * @return A listing of all CMS data types.
     */
    @Cacheable(value = "typeCache", key = "#root.targetClass")
    @Query("select type from CmsType type")
    List<CmsType> getAllTypes();
}
