package company.repository;

import company.entity.Admin;
import company.entity.Employee;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 1/13/16.
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
    /**
     * getEmployees: build a listing of all employees.
     * @return List of all employees.
     */
    @Query("select emp from Employee emp")
    @Cacheable(value = "employeeCache", key = "#root.targetClass")
    List<Employee> getEmployees();

    /**
     * Employee lookup method - parse the employee table for specific values.
     * @param value - search value we're looking up.
     * @return List of employee data located from the search value.
     */
    @Query("select emp from Employee emp where emp.name like :value or emp.login like :value or emp.position like :value")
    List<Employee> findEmployee(String value);
}
