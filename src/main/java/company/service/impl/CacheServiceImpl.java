package company.service.impl;

import company.service.CacheService;
import net.sf.ehcache.CacheManager;
import org.springframework.stereotype.Service;

/**
 * Created by cgrieb on 10/30/16.
 */
@Service
public class CacheServiceImpl implements CacheService {
    @Override
    public void clearCache() {
        CacheManager cacheManager = new CacheManager();
        cacheManager.clearAll();
    }

}
