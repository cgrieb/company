package company.service.impl;

import company.entity.Employee;
import company.repository.EmployeeRepository;
import company.service.EmployeeService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by christopherg27 on 12/26/15.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Resource
    EmployeeRepository employeeRepository;

    @CacheEvict(value = "employeeCache", allEntries = true)
    public void addEmployee(String avatar, String login, String name, String position,int salary) {
        Employee employee = new Employee();
        employee.setLogin(login);
        employee.setPosition(position);
        employee.setAvatar(avatar);
        employee.setName(name);
        employee.setSalary(salary);

        employeeRepository.save(employee);
    }

    @CacheEvict(value = "employeeCache", allEntries = true)
    public void saveEmployee(int id,String login, String name, String position,int salary) {
        Employee employee = employeeRepository.findOne(id);
        employee.setLogin(login);
        employee.setName(name);
        employee.setPosition(position);
        employee.setSalary(salary);

        employeeRepository.save(employee);
    }

    public List<Employee> getEmployees() {
        return employeeRepository.getEmployees();
    }

    public List<Employee> findEmployee(String value) {
        return employeeRepository.findEmployee(value);
    }

    @CacheEvict(value = "employeeCache", allEntries = true)

    public void removeEmployee(int id) {
        Employee employee = employeeRepository.findOne(id);
        employeeRepository.delete(employee);
    }

}