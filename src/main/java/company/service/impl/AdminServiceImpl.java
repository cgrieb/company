package company.service.impl;

import company.encoder.StringToSHA256Encoder;
import company.entity.Admin;
import company.entity.Roles;
import company.repository.AdminRepository;
import company.repository.RoleRepository;
import company.service.AdminService;
import company.service.AuthService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by cgrieb on 1/3/16.
 */
@Service
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminRepository adminRepository;
    @Resource
    private RoleRepository roleRepository;

    private static final StringToSHA256Encoder STRING_TO_SHA_256_ENCODER = new StringToSHA256Encoder();
    private static final Logger LOGGER = Logger.getLogger(AuthService.class.getName());

    @CacheEvict(value = "adminCache", allEntries = true)
    public void adminAdd(String password,String role,String username) {
        Admin admin = adminRepository.findOne(username);
        if(admin==null) {
            admin.setUsername(username);
            admin.setRole(role);
            admin.setEnabled(true);
            admin.setPassword(STRING_TO_SHA_256_ENCODER.encode(password));
            adminRepository.save(admin);
        } else {
            LOGGER.warning("** admin " + username + " already exists **");
        }
    }

    public List<Roles> getRoleListing() {
        return roleRepository.getRoles();
    }

    public  List<Admin> getAllAdmins() {
        return adminRepository.getAllAdmins();
    }

    @CacheEvict(value = "adminCache", allEntries = true)
    public void saveAdmin(boolean enabled, String password, String role, String username) {
        Admin admin = adminRepository.findOne(username);
        admin.setEnabled(enabled);
        admin.setRole(role);

        if(this.updatePassword(admin.getPassword(),password)) {
            admin.setPassword(STRING_TO_SHA_256_ENCODER.encode(password));
        }

        adminRepository.save(admin);
    }

    /**
     * Determine if we have a change to the password.
     * @param current - current password.
     * @param updated - new password.
     * @return updated/not updated, true/false.
     */
    private boolean updatePassword(String current, String updated) {
        return !(current.equals(updated));
    }
}
