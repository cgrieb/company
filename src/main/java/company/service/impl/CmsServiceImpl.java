package company.service.impl;

import company.entity.CmsItem;
import company.entity.CmsType;
import company.repository.CmsItemRepository;
import company.repository.CmsTypeRepository;
import company.service.CmsService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by cgrieb on 4/18/16.
 */
@Service
public class CmsServiceImpl implements CmsService {
    @Resource
    CmsTypeRepository cmsTypeRepository;

    @Resource
    CmsItemRepository cmsItemRepository;

    private static final String CMS_CACHE = "cmsCache";

    public List<CmsType> getAllTypes() {
        return cmsTypeRepository.getAllTypes();
    }

    @CacheEvict(value = CMS_CACHE,allEntries = true)
    public void addContent(String content, boolean itemIsLIve, boolean publishItem, String title, String type, UserDetails userDetails) {
        CmsItem cmsItem = new CmsItem();
        cmsItem.setData(content);
        cmsItem.setLive(itemIsLIve);
        cmsItem.setPublished(publishItem);
        cmsItem.setTitle(title);
        cmsItem.setType(type);
        cmsItem.setCreator(userDetails.getUsername());

        cmsItemRepository.save(cmsItem);
    }

    @CacheEvict(value = CMS_CACHE,allEntries = true)
    public void saveExistingItem(String data, int id,boolean live,boolean published,String title) {
        CmsItem cmsItem = cmsItemRepository.findOne(id);
        cmsItem.setData(data);
        cmsItem.setLive(live);
        cmsItem.setPublished(published);
        cmsItem.setTitle(title);

        cmsItemRepository.save(cmsItem);
    }

    public List<CmsItem> searchForItems(String searchValue) {
        return cmsItemRepository.searchForItems(searchValue);
    }

    public List<CmsItem> getAllCmsItems() {
        return cmsItemRepository.getAllItems();
    }
}
