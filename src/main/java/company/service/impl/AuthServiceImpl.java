package company.service.impl;

import java.util.logging.Logger;

import company.service.AuthService;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 10/22/16.
 */
@Service
public class AuthServiceImpl implements AuthService {
    @Resource
    AuthenticationManager authenticationManager;

    private static final Logger LOGGER = Logger.getLogger(AuthService.class.getName());

    @Override
    public boolean login(String username, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username,password);
        try {
            try {
                Authentication authentication = authenticationManager.authenticate(token);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                if (authentication.isAuthenticated()) {
                    return true;
                }
            } catch (DisabledException e) {
                LOGGER.warning(e.getMessage());
            }

        } catch (BadCredentialsException e) {
            LOGGER.warning(e.getMessage());
        }
        return false;
    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Override
    public boolean getAuthStatus() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !(authentication instanceof AnonymousAuthenticationToken);
    }

    @Override
    public UserDetails getUserDetails() {
        return (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Override
    public boolean isAdmin(HttpServletRequest request) {
        return request.isUserInRole("admin");
    }
}
