package company.service;

import company.entity.Admin;
import company.entity.Roles;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by cgrieb on 1/3/16.
 */
public interface AdminService {
    /**
     * adminAdd - calls AdminRepository.
     * @param username username of the admin.
     * @param password password of the admin.
     * @param role role of the admin.
     */
    void adminAdd(String password,String role,String username);

    /**
     * getRoleListing - calls AdminRepository.
     * @return List of roles.
     */
    List<Roles> getRoleListing();

    /**
     * getAllAdmins - calls AdminRepository.
     * @return List of admins.
     */
    List<Admin> getAllAdmins();

    /**
     * saveAdmin - save an existing admin.
     * @param enabled - are we enabling/disabling this guy?
     * @param password - password.
     * @param role - role (user/admin)
     * @param username username.
     * @return true/false, updated/not updated.
     */
    void saveAdmin(boolean enabled, String password, String role, String username);
}
