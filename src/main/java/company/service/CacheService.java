package company.service;

/**
 * Created by cgrieb on 10/30/16.
 */
public interface CacheService {
    /**
     * Clear all system caches.
     */
    void clearCache();
}
