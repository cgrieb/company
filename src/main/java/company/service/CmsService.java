package company.service;
import company.entity.CmsItem;
import company.entity.CmsType;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * Created by cgrieb on 4/18/16.
 */
public interface CmsService {
    /**
     * Get all CMS item types.
     * @return a list of all item types.
     */
    List<CmsType> getAllTypes();

    /**
     *
     * @param content
     * @param itemIsLIve
     * @param publishItem
     * @param title
     * @param type
     * @param userDetails
     * @return
     */
    void addContent(String content, boolean itemIsLIve, boolean publishItem, String title, String type, UserDetails userDetails);

    /**
     *
     * @param searchValue
     * @return
     */
    List<CmsItem> searchForItems(String searchValue);

    void saveExistingItem(String data, int id,boolean live,boolean published,String title);

    /**
     *
     * @return
     */
    List<CmsItem> getAllCmsItems();
}
