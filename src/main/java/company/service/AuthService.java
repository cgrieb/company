package company.service;

import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 10/22/16.
 */
public interface AuthService {
    /**
     * Service for logging an admin into the system.
     * @param username - username (e.g, 'ljohnson').
     * @param password - password.
     * @return logged in successfully/not logged int successfully.
     */
    boolean login(String username, String password);

    /**
     * Service for logging an admin out of the system.
     */
    void logout();

    /**
     * Get authenticated status.
     * @return authenticated user/not authenticated user.
     */
    boolean getAuthStatus();

    /**
     * Return UserDetails regarding logged in admin - note: this service has a secured
     * entry point.
     * @return UserDetails object.
     */
    UserDetails getUserDetails();

    /**
     * Determine if the user is in the 'admin' roles group.
     * @param request - standard HttpServletRequest
     * @return is admin/is not admin.
     */
    boolean isAdmin(HttpServletRequest request);
}
