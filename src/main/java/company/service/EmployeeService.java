package company.service;

import company.entity.Employee;

import java.util.List;

/**
 * Created by christopherg27 on 12/26/15.
 */
public interface EmployeeService {
    /**
     * addEmployee - calls the repository.
     * @param name - name of the employee.
     * @param position - position of the employee.
     * @param salary - salary of the employee.
     */
   void addEmployee(String avatar, String login, String name, String position,int salary);

    /**
     * SaveEmployee - save a new employee.
     * @param id - employee ID.
     * @param login - employee login.
     * @param name - employee name (e.g, 'Bob Johnson').
     * @param position - employee position.
     * @param salary - employee salary.
     */
    void saveEmployee(int id,String login, String name, String position,int salary);

    /**
     * getEmployees - calls EmployeeRepository.
     * @return List of Employees.
     */
    List<Employee> getEmployees();

    /**
     * findEmployee - calls EmployeeRepository.
     * @param value - employee ID.
     * @return object of type Employee.
     */
    List<Employee> findEmployee(String value);

    /**
     * removeEmployee - calls EmployeeRepository.
     * @param id - employee ID to remove.
     */
    void removeEmployee(int id);

}
