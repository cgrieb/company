What you'll need to get this project up and running:
1. Java 8.
2. Tomcat and Maven.
3. IntelliJ or Eclipse.
4. SQL Server.

Steps to configure the project to work in your environment:
1. Open the project with IntelliJ (browse to the git repository in IntelliJ and click on the pom.xml file).
2. Edit the src/webapp/WEB-INF/mvc-dispatches-servlet.xml file.
3. Edit the dataSourceMetropolis bean and change the url, password, and username properties to match your SQL server configuration.
4. Execute the src/sql/company.ddl and src/sql/company.dml SQL scripts (in that order) to create the data structure.
5. Setup IntelliJ to utilize your tomcat/maven setup.
6. In intelliJ, select the Maven Properties tab on the right hand side, and run a maven clean && maven compile (mvn clean/mvn compile).
7. Finally, build and run the project, navigate to http://localhost:8080
